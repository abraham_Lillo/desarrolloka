from django.urls import path
from .views import home, contacto, nosotros, servicios
urlpatterns = [
    path('', home, name="home"),
    path('contacto/', contacto, name="contacto"),
    path('nosotros/', nosotros, name="nosotros"),
    path('servicios/', servicios, name="servicios"),
]
