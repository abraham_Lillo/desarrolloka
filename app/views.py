from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'app/home.html')


def contacto(request):
    return render(request, 'app/contacto.html')


def nosotros(request):
    return render(request, 'app/nosotros.html')  


def servicios(request):
    return render(request, 'app/servicios.html')        